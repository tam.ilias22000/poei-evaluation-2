package com.zenika.poei.amismaisonduvin.domain;

import com.zenika.poei.amismaisonduvin.domain.exception.InvalidReviewException;
import com.zenika.poei.amismaisonduvin.domain.exception.InvalidWineException;
import org.springframework.util.StringUtils;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

public class Wine {

    private Integer id;
    private String appellation;
    private String region;
    private Year vintage;
    private String colour;

    private List<Review> reviews = new ArrayList<>();

    public Wine() {
    }

    public Wine(String appellation, Year vintage, String region, String colour, String variety) {
        this(null, appellation, vintage, region, colour, variety, new ArrayList<>());
    }

    public Wine(Integer id, String appellation, String region, String colour) {
        this.id = id;
        this.appellation = appellation;
        this.region = region;
        this.colour = colour;
    }

    public Wine(Integer id, String appellation, Year vintage, String region, String colour, String variety, List<Review> reviews) {
        setId(id);
        setAppellation(appellation);
        setRegion(region);
        setVintage(vintage);
        setColour(colour);
        setReviews(reviews);
        setVariety(variety);
    }

    /**
     * It is the grape variety :
     */
    private String variety;


    public Wine(int id, String appellation, String region, Year vintage, String colour) {
        this.id = id;
        this.appellation = appellation;
        this.region = region;
        this.vintage = vintage;
        this.colour = colour;
    }

    public Integer getId() {
        return id;
    }

    public Wine setId(Integer id) {
        this.id = id;

        return this;
    }

    public String getAppellation() {
        return appellation;
    }

    public Wine setAppellation(String appellation) {
        if (!StringUtils.hasText(appellation)) {
            throw new InvalidWineException("Appellation is empty");
        }

        this.appellation = appellation;

        return this;
    }

    public String getRegion() {
        return region;
    }

    public Wine setRegion(String region) {
        if (!StringUtils.hasText(region)) {
            throw new InvalidWineException("Region is empty");
        }

        this.region = region;

        return this;
    }

    public Year getVintage() {
        return vintage;
    }

    public Wine setVintage(Year vintage) {
        if (Year.now().isBefore(vintage)) {
            throw new InvalidWineException("Vintage " + vintage + " is in future");
        }

        this.vintage = vintage;

        return this;
    }

    public String getColour() {
        return colour;
    }

    public Wine setColour(String colour) {
        if (!StringUtils.hasText(colour)) {
            throw new InvalidWineException("Colour is empty");
        }

        this.colour = colour;

        return this;
    }

    public String getVariety() {
        return variety;
    }

    public Wine setVariety(String variety) {
        if (!StringUtils.hasText(variety)) {
            throw new InvalidWineException("Variety is empty");
        }

        this.variety = variety;

        return this;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        if (reviews == null) {
            throw new InvalidWineException("Reviews is undefined");
        }

        reviews = List.copyOf(reviews);
    }

    public void addReview(Review review) {
        if (review == null) {
            throw new InvalidReviewException("Review is undefined");
        }

        reviews.add(review);
    }

    public Review getReviewFromMember(Member member) {
        for (Review review : reviews) {
            if(review.getMember().equals(member)){
                return review;
            }
        }
        return null;
    }
}
