package com.zenika.poei.amismaisonduvin.dto;

import com.zenika.poei.amismaisonduvin.domain.Review;

public class ReviewDto {

    private Integer memberId;
    private int score;
    private String comment;

    public ReviewDto(Integer memberId, int score, String comment) {
        this.memberId = memberId;
        this.score = score;
        this.comment = comment;
    }

    public ReviewDto() {
    }

    public Integer getMemberId() {
        return memberId;
    }

    public int getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public static ReviewDto fromDomain(Review review) {

        ReviewDto reviewDto = new ReviewDto();
        reviewDto.setMemberId(review.getMember().getId());
        reviewDto.setScore(review.getScore());
        reviewDto.setComment(review.getComment());
        return reviewDto;
    }
}
