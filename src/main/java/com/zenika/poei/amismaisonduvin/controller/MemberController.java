package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MemberController {

    private final JdbcTemplate jdbcTemplate;
    private final MemberRepository memberRepository;


    @Autowired
    public MemberController(JdbcTemplate jdbcTemplate, MemberRepository memberRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.memberRepository = memberRepository;
    }

    @GetMapping("members")
    public List<Member> getMembers(@RequestParam(required = false) String firstname) {

        if (firstname == null){
            return memberRepository.getAll();
        }

        return memberRepository.getMemberByName(firstname);

    }
    @GetMapping("/members/{id}")
    public Member getMemberById(@PathVariable("id") int id){
        return memberRepository.getById(id);
    }

    @PostMapping("/members")
    public Member postProduct(@RequestBody Member member ){

        return memberRepository.createMember(member);
    }

    @PutMapping("/members/{id}")
    public void updateProduct(@PathVariable ("id") int id, @RequestBody Member newMember){

        memberRepository.updateMember(id,newMember);
    }

    @DeleteMapping("/members/{id}")
    public void deleteProduct(@PathVariable ("id") int id) {

        memberRepository.deleteMember(id);
    }

}
