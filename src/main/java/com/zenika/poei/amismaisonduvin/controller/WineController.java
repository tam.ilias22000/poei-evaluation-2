package com.zenika.poei.amismaisonduvin.controller;


import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.dto.ReviewDto;
import com.zenika.poei.amismaisonduvin.repository.MemberRepository;
import com.zenika.poei.amismaisonduvin.repository.ReviewRepository;
import com.zenika.poei.amismaisonduvin.repository.WineRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WineController {
    private final JdbcTemplate jdbcTemplate;
    private final WineRepository wineRepository;
    private final MemberRepository memberRepository;
    private final ReviewRepository reviewRepository;

    public WineController(JdbcTemplate jdbcTemplate, WineRepository wineRepository, MemberRepository memberRepository, ReviewRepository reviewRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.wineRepository = wineRepository;
        this.memberRepository = memberRepository;
        this.reviewRepository = reviewRepository;
    }

    @GetMapping("wines")
    public List<Wine> getWine(@RequestParam(required = false) String region){
        if(region == null){
            return wineRepository.getAll();
        }
        return wineRepository.getWineByRegion(region);
    }

    @GetMapping("/wines/{id}")
    public Wine getWineById(@PathVariable("id") int id){
        return wineRepository.getWineById(id);
    }

    @PostMapping("wines")
    public Wine addWine(@RequestBody Wine wine){
        return wineRepository.save(wine);
    }

    @PostMapping("/wines/{id}/reviews")
    public Wine addReview(@PathVariable int id, @RequestBody Review newReview) {
        Review review = new Review(
                memberRepository.getById(newReview.getMember().getId())
                , newReview.getScore()
                , newReview.getComment());
        Wine wine = wineRepository.getWineById(id);
        wine.addReview(review);

        return wineRepository.save(wine);
    }




    @DeleteMapping("/wines/{id}")
    public void deleteWine(@PathVariable ("id") int id) {

        wineRepository.deleteWine(id);
    }

    @DeleteMapping("wines/{wineId}/{memberId}")
    public  void  deleteMemberFromWine(@PathVariable int wineId,@PathVariable int memberId){
        Member member = memberRepository.getById(memberId);
        Wine wine = wineRepository.getWineById(wineId);
        Review review = wine.getReviewFromMember(member);
        wine.getReviews().remove(review);
        reviewRepository.deleteReview(memberId);
    }



}
