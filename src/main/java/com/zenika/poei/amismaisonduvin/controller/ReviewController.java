package com.zenika.poei.amismaisonduvin.controller;

import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.repository.ReviewRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReviewController {

    private final JdbcTemplate jdbcTemplate;
    private final ReviewRepository reviewRepository;

    public ReviewController(JdbcTemplate jdbcTemplate, ReviewRepository reviewRepository) {
        this.jdbcTemplate = jdbcTemplate;
        this.reviewRepository = reviewRepository;
    }

    @GetMapping("reviews")
    public List<Review> getAll(){
        return reviewRepository.getAll();
    }
}
