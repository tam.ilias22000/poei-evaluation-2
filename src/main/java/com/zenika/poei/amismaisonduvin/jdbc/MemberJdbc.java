package com.zenika.poei.amismaisonduvin.jdbc;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.repository.MemberRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


@Component
public class MemberJdbc implements MemberRepository {

    private final JdbcTemplate jdbcTemplate;

    public MemberJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    //return new Member(rs.getInt("id"), rs.getString("pseudonym"),
    //                    rs.getString("firstName"), rs.getString("lastName"));

    RowMapper<Member> rowMapper = new RowMapper<Member>() {
        @Override
        public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Member(rs.getInt("id"),
                    rs.getString("pseudonym"),
                    rs.getString("firstName"),
                    rs.getString("lastName"));
        }
    };

    @Override
    public List<Member> getAll() {
        String sql = "select * from member";
        return jdbcTemplate.query(sql,rowMapper );
    }

    @Override
    public Member getById(int id) {
        String sql = "select * from member where id = ?";
        return jdbcTemplate.queryForObject(sql, rowMapper, id);
    }

    @Override
    public List<Member> getMemberByName(String firstname) {
        String sql = "select * from member where firstname = ?";
        return jdbcTemplate.query(sql, rowMapper, firstname);
    }

    @Override
    public Member createMember(Member member) {
        String sql = "insert into member (pseudonym,firstName,lastName) values (?, ?, ?) returning *";
        return  jdbcTemplate.queryForObject(sql,rowMapper,member.getPseudonym(), member.getFirstName(),member.getLastName());
    }

    @Override
    public void updateMember(int id, Member newMember) {
        String sql = "update member set firstname = ? where id = ?";
        jdbcTemplate.update(sql,newMember.getFirstName(), id);
    }

    @Override
    public void deleteMember(int id) {
        String sql = "delete from member where id = ?";
        jdbcTemplate.update(sql,id);
    }
}
