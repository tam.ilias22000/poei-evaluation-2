package com.zenika.poei.amismaisonduvin.jdbc;

import com.zenika.poei.amismaisonduvin.domain.Member;
import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.repository.ReviewRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class ReviewJdbc implements ReviewRepository {

    private final JdbcTemplate jdbcTemplate;

    public ReviewJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    public static RowMapper<Review> rowMapper = new RowMapper<Review>() {
        @Override
        public Review mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Review(
                    new Member(rs.getInt("member_id"),
                            rs.getString("pseudonym"),
                            rs.getString("firstname"),
                            rs.getString("lastname")),
                    rs.getInt("score"),
                    rs.getString("comment")
            );
        }
    };

    @Override
    public Review createReview(Review review, Wine wine) {

        String sql = "insert into review (wine_id, member_id, score, comment) values (?,?,?,?) returning *";
        return jdbcTemplate.queryForObject(sql,rowMapper,
                wine.getId(),
                review.getMember().getId(),
                review.getScore(),
                review.getComment());
    }

    @Override
    public List<Review> getAll() {

        String sql = "select * from review left join member m on m.id = review.member_id";
        return jdbcTemplate.query(sql,rowMapper);
    }

    @Override
    public void updateReview(Review review, Wine wine) {

        String sql = "update review set comment = ? where member_id = ? and wine_id = ?";
        jdbcTemplate.update(sql,
                review.getComment(),
                review.getMember().getId(),
                wine.getId());
    }

    @Override
    public void deleteReview(int id) {

        String sql = "delete from review where member_id = ?";
        jdbcTemplate.update(sql,id);

    }
}
