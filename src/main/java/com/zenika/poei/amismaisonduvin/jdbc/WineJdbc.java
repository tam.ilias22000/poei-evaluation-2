package com.zenika.poei.amismaisonduvin.jdbc;

import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;
import com.zenika.poei.amismaisonduvin.repository.WineRepository;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Year;
import java.util.List;

@Component
public class WineJdbc implements WineRepository {

    private final JdbcTemplate jdbcTemplate;

    public WineJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    RowMapper<Wine> rowMapper = new RowMapper<Wine>() {//
        @Override
        public Wine mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Wine (rs.getInt("id"),
                    rs.getString("appellation"),
                    rs.getString("region"),
                    Year.of(rs.getInt("vintage")),
                    rs.getString("colour")
            );
        }
    };

    @Override
    public List<Wine> getAll() {

        List<Wine> wines = jdbcTemplate.query("select * from wine",rowMapper);
        for (Wine wine : wines) {
            String sqlWine = "select * from review left join member m on m.id = review.member_id where wine_id = ?";
            List<Review> reviews = jdbcTemplate.query(sqlWine,ReviewJdbc.rowMapper,wine.getId());
            reviews.forEach(wine::addReview);

        }
        return wines;
    }

    @Override
    public Wine getWineById(int id) {


        String sql = "select * from wine left join review r on wine.id = r.wine_id left join member m on m.id = r.member_id  where wine_id = ?";
        List<Review> reviews = jdbcTemplate.query(sql,ReviewJdbc.rowMapper,id);

        Wine wine = jdbcTemplate.queryForObject("select * from wine where id = ?", rowMapper,id);
        reviews.forEach(wine::addReview);
        return wine;
    }

    public void updateWine(Wine wine) {

        String sql = "update wine set region = ? where id = ?";
        jdbcTemplate.update(sql,wine.getRegion(),wine.getId());
    }

    private Wine createNewStatusWine(Wine wine) {
        String sql = "insert into wine (appellation,region,vintage,colour) values (?,?,?,?) returning *";
        return  jdbcTemplate.queryForObject(sql,
                rowMapper,
                wine.getAppellation(),
                wine.getRegion(),
                wine.getVintage().getValue(),
                wine.getColour());
    }

    @Override
    public Wine save(Wine wine) {
        if(wine.getId() == null) {
            return createNewStatusWine(wine);
        } else {

            updateWine( wine);
            return wine;
        }

    }

    @Override
    public void deleteWine(int id) {
        String sql = "delete from wine where id = ?";
        jdbcTemplate.update(sql,id);
    }

    @Override
    public List<Wine> getWineByRegion(String region) {
        List<Wine> wines = jdbcTemplate.query("select * from wine where region = ?", rowMapper,region);

        for (Wine wine : wines) {
            String sql = "select * from wine left join review r on wine.id = r.wine_id left join member m on m.id = r.member_id  where wine_id = ?";
            List<Review> reviews = jdbcTemplate.query(sql,ReviewJdbc.rowMapper,wine.getId());
            reviews.forEach(wine::addReview);
        }
        return wines;
    }


}
