package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Wine;

import java.util.List;

public interface WineRepository {

    List<Wine> getAll();

    Wine getWineById(int id);

    Wine save(Wine wine);

    void deleteWine(int id);

    List<Wine> getWineByRegion(String region);
}
