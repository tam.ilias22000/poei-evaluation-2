package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Review;
import com.zenika.poei.amismaisonduvin.domain.Wine;

import java.util.List;

public interface ReviewRepository {

    Review createReview(Review review, Wine wine);

    List<Review> getAll();

    void updateReview(Review Review, Wine wine);

    void deleteReview(int id);
}
