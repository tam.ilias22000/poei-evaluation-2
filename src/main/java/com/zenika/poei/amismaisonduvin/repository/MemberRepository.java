package com.zenika.poei.amismaisonduvin.repository;

import com.zenika.poei.amismaisonduvin.domain.Member;

import java.util.List;

public interface MemberRepository {

    List<Member> getAll();

    Member getById(int id);

    List<Member> getMemberByName(String name);

    Member createMember(Member member);

    void updateMember(int id, Member newMember);

    void deleteMember(int id);


}
