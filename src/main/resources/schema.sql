create table IF NOT EXISTS member  (
                                       id integer generated always as identity primary key ,
                                       pseudonym text not null unique,
                                       firstName text not null ,
                                       lastName text not null );

create table IF NOT EXISTS wine (
                                    id integer generated always as identity primary key ,
                                    appellation text not null ,
                                    region text not null ,
                                    vintage int not null ,
                                    colour text not null

);

create table IF NOT EXISTS review
(
    member_id integer not null references member(id),
    wine_id integer not null references wine(id),
    score integer not null,
    comment text not null ,
    primary key (wine_id, member_id)
);





